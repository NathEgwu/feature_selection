# README #

The aim of this code to adapt the Levenberg-Marquardt Algorithm via a continuous feature selection technique. The method is to incorporate the
the structured l2 penalty. The implementation is done by adapting the back-propogation algorithm in TensorFlow. Data preparation was performed via unsupervised learning (clustering, normalization, imputation and feature ranking), correlation;
and the adaptive training was performed by by training an Artificial Neural Network structure via deep learning.
### What is this repository for? ###

* Part of PhD project for structured penalty for sparse model reconstruction
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Essential Tools? ###

* Data preparation using Scikit-Learn, Imputation, Correlation Analysis and Normalization
* Unsupervised learning using clustering, etc
* Python, Pandas, 
* Tensorflow, Java (when necessary), etc
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing other implementations
* Code review
* Validate models using different data types
* Analyze results using different benchmarks

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact